package com.example.customerclient;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@DirtiesContext
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids = "com.example:customer-service:+:8081", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
public class CustomerClientContractTest {

    @Test
    public void clientAllCustomers() {
        final CustomerClient client = new CustomerClient();
        final Collection<Customer> result = client.getAllCustomers();
        assertThat(result.size(), is(2));
        assertThat(result.iterator().next().getId(), is(1L));
        assertThat(result.iterator().next().getName(), is("Luis"));
    }
}