package com.example.customerclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.apache.http.HttpHeaders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@DirtiesContext
@RunWith(SpringRunner.class)
@AutoConfigureWireMock(port = 8081)
public class CustomerClientTest {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void clientAllCustomers() throws JsonProcessingException {

        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/customers"))
                .willReturn(
                        WireMock.aResponse()
                                .withStatus(200)
                                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                                .withBody(jsonFromCustomers(new Customer(1L, "Luis"),
                                        new Customer(2L, "Lily")))
                ));

        final CustomerClient client = new CustomerClient();
        final Collection<Customer> result = client.getAllCustomers();
        assertThat(result.size(), is(2));
        assertThat(result.iterator().next().getId(), is(1L));
        assertThat(result.iterator().next().getName(), is("Luis"));
    }

    private String jsonFromCustomers(Customer... customers) throws JsonProcessingException {
        List<Customer> customerList = Arrays.asList(customers);
        return objectMapper.writeValueAsString(customerList);
    }
}