### Stub Runner Server Fat Jar

You can download the standalone JAR with wgetÑ

```
$ wget -O stub-runner.jar 'https://search.maven.org/remote_content?g=org.springframework.cloud&a=spring-cloud-contract-stub-runner-boot&v=1.2.4.RELEASE'
$ java -jar stub-runner.jar --stubrunner.ids=... --stubrunner.repositoryRoot=...
$ java -jar stub-runner.jar --stubrunner.ids='com.example:customer-service:+:8081' --stubrunner.workOffline=true
```
